﻿using PerformanceCounterHelper;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceCounterTest
{
    [PerformanceCounterCategoryAttribute("TestApplication", 
    	System.Diagnostics.PerformanceCounterCategoryType.MultiInstance,
    	"Detail information about the test application.")]
    public enum QueueCounter
    {
        [PerformanceCounter("Read count","Count the number of item read",PerformanceCounterType.SampleCounter)]
        ItemRead,
        [PerformanceCounter("Heart beat", "Signals the component is alive", PerformanceCounterType.NumberOfItems32)]
        HeartBeat,
    }
}
