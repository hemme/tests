﻿using PerformanceCounterHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerformanceCounterTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var r = new Random();
            using (var counter = PerformanceHelper.CreateCounterHelper<QueueCounter>("An instance"))
            {
                try
                {
                    while (true)
                    {
                        try
                        {
                            counter.Reset(QueueCounter.ItemRead);
                            if (DateTime.Now.Second%3==0)
                                counter.IncrementBy(QueueCounter.ItemRead, r.Next(50));

                            counter.IncrementBy(QueueCounter.HeartBeat,
                                DateTime.Now.Second % 2 == 0 ? 1 : 0
                                );
                        }
                        catch { }
                        System.Threading.Thread.Sleep(1000);
                    }
                }
                finally
                {
                    counter.Reset(QueueCounter.ItemRead);
                }
            }
            
        }
    }
}
